package ua.kh.kroha.tests;


import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import ua.kh.kroha.utils.Random;
import ua.kh.kroha.data.UserData;
import ua.kh.kroha.pages.FAQPage;
import ua.kh.kroha.pages.PageBase;

import static com.codeborne.selenide.Selenide.open;

public class TestCase extends TestBase {

    private Random random = new Random();
    private FAQPage faqPage = new FAQPage();
    private UserData data = new UserData();
    private PageBase pageBase = new PageBase();

    @Test
    public void userCanGetCallBackFromFAQPage() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.nameInput.setValue(random.generateRandomString());
        pageBase.phoneInput.val(data.userPhone);
        faqPage.writeMessage(random.generateRandomEmail());
        pageBase.termsCheckBox.click();
        pageBase.submitButton.click();
        pageBase.successMessage.shouldBe(Condition.visible);
    }
}


