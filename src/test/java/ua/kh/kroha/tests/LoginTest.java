package ua.kh.kroha.tests;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ua.kh.kroha.data.Validators;
import ua.kh.kroha.utils.Random;
import ua.kh.kroha.data.UserData;
import ua.kh.kroha.pages.PageBase;

import static com.codeborne.selenide.Selenide.open;

public class LoginTest extends TestBase {
    private PageBase pageBase = new PageBase();
    private UserData data = new UserData();
    private Random random = new Random();
    private Validators validators = new Validators();


    @Test
    public void userCanLogin(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canLoginAsUser(
                random.generateRandomEmail(),
                data.password);
        pageBase.successMessage.shouldBe(Condition.visible);
    }

    @Test
    public void userCanRegister(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                random.generateRandomEmail(),
                data.userPhone,
                data.password,
                data.passwordRepeat);
        pageBase.successMessage.shouldBe(Condition.visible);
    }

    @Test
    public void wholesaleCanLogin(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canLoginAsUser(
                data.userEmail,
                data.password);
        pageBase.successMessage.shouldBe(Condition.visible);
    }

    @Test
    public void wholesaleCanRegister(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.userPhone,
                data.password,
                data.passwordRepeat);
        pageBase.successMessage.shouldBe(Condition.visible);
    }

//    Негативные сценарии

    @Test
    public void userCanNotRegisterWithWrongName(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.badName,
                data.userSurname,
                random.generateRandomEmail(),
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalNameError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotRegisterWithWrongSurname(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.badSurname,
                random.generateRandomEmail(),
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalSurnameError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotRegisterWithWrongEmail(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.badEmail,
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalEmailError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotRegisterWithWrongPhoneNumber(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.badPhone,
                data.password,
                data.passwordRepeat);
        validators.telError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotRegisterWithIncorrectPassword(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.userPhone,
                data.badPassword,
                data.badPassword);
        validators.modalPasswordError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Ignore
    @Test
    public void userCanNotRegisterWithIncorrectConfirmationOfPassword(){
        open(baseUrl);
        pageBase.openUserAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.userPhone,
                data.password,
                data.badConfirmationPassword);
        validators.modalRepeatPasswordError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void wholesaleCanNotRegisterWithWrongName(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.badName,
                data.userSurname,
                random.generateRandomEmail(),
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalNameError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void wholesaleCanNotRegisterWithWrongSurname(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.badSurname,
                random.generateRandomEmail(),
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalSurnameError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void wholesaleCanNotRegisterWithWrongEmail(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.badEmail,
                data.userPhone,
                data.password,
                data.passwordRepeat);
        validators.modalEmailError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void wholesaleCanNotRegisterWithWrongPhoneNumber(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.badPhone,
                data.password,
                data.passwordRepeat);
        validators.telError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Test
    public void wholesaleCanNotRegisterWithIncorrectPassword(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.userPhone,
                data.badPassword,
                data.badPassword);
        validators.modalPasswordError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }

    @Ignore
    @Test
    public void wholesaleCanNotRegisterWithIncorrectConfirmationOfPassword(){
        open(baseUrl);
        pageBase.openWholesaleAuthModal();
        pageBase.canRegisterAsUser(
                data.userName,
                data.userSurname,
                data.userEmail,
                data.userPhone,
                data.password,
                data.badConfirmationPassword);
        validators.modalRepeatPasswordError.shouldBe(Condition.visible);
        pageBase.successMessage.shouldNotBe(Condition.visible);
    }
}
