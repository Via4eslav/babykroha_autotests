package ua.kh.kroha.tests;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import ua.kh.kroha.data.UserData;
import ua.kh.kroha.data.Validators;
import ua.kh.kroha.pages.FAQPage;
import ua.kh.kroha.pages.PageBase;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CallBackTest extends TestBase {
    private PageBase pageBase = new PageBase();
    private UserData data = new UserData();
    private FAQPage faqPage = new FAQPage();
    private Validators validators = new Validators();

    @Test
    public void userCanGetCallBack() {
        open(baseUrl);
        pageBase.canGetCallBack(
                data.userName,
                data.userPhone);
        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }

    @Test
    public void userCanGetCallBackFromFAQPage() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.nameInput.setValue(data.userName);
        faqPage.phoneInput.setValue(data.userPhone);
        faqPage.writeMessage(data.testMessage);
        pageBase.termsCheckBox.click();
        pageBase.submitButton.click();
        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }

//    Негативные сценарии

    @Test
    public void userCanNotGetCallBackWithWrongName() {
        open(baseUrl);
        pageBase.canGetCallBack(
                data.badName,
                data.userPhone);
        validators.modalNameError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackWithWrongPhoneNumber() {
        open(baseUrl);
        pageBase.canGetCallBack(
                data.userName,
                data.badPhone);
        validators.modalTelError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanGetCallBackWithEmptyFields() {
        open(baseUrl);
        pageBase.emptyFieldsInCallbackForm();
        validators.modalNameError.shouldBe(Condition.visible);
        validators.modalTelError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackWithoutCheckedPersonalDataRules() {
        open(baseUrl);
        pageBase.callbackModal.click();
        pageBase.nameInput.val(data.userName);
        pageBase.telInput.val(data.userPhone);
        pageBase.submitButton.click();
        validators.termsError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackFromFAQPageWithBadName() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.nameInput.val(data.badName);
        faqPage.phoneInput.val(data.userPhone);
        faqPage.writeMessage(data.testMessage);
        pageBase.termsCheckBox.click();
        pageBase.submitButton.click();
        validators.modalNameError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackFromFAQPageWithBadPhoneNumber() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.nameInput.val(data.userName);
        faqPage.phoneInput.val(data.badPhone);
        faqPage.writeMessage(data.testMessage);
        pageBase.termsCheckBox.click();
        pageBase.submitButton.click();
        validators.modalTelError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackFromFAQPageWithEmptyFields() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.submitButton.click();
        validators.modalNameError.shouldBe(Condition.visible);
        validators.modalTelError.shouldBe(Condition.visible);
        validators.termsError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotGetCallBackFromFAQPageWithoutCheckedPersonalDataRules() {
        open(baseUrl + "/faq.html");
        faqPage.pageTitleFAQ.shouldHave(Condition.text("помощь"));
        faqPage.openFAQModal();
        pageBase.nameInput.val(data.userName);
        faqPage.phoneInput.val(data.userPhone);
        faqPage.writeMessage(data.testMessage);
        pageBase.submitButton.click();
        validators.termsError.shouldBe(Condition.visible);
//        pageBase.reviewPopUp.shouldBe(Condition.visible);
    }
}
