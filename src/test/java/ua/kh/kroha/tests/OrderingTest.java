package ua.kh.kroha.tests;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import ua.kh.kroha.data.UserData;
import ua.kh.kroha.data.Validators;
import ua.kh.kroha.pages.PageBase;
import ua.kh.kroha.pages.ProductPage;

import static com.codeborne.selenide.Selenide.open;
import static ua.kh.kroha.data.UserData.*;

public class OrderingTest extends TestBase {
    private PageBase pageBase = new PageBase();
    private ProductPage productPage = new ProductPage();
    private UserData data = new UserData();
    private Validators validators = new Validators();

    @Test
    public void userCanOrderFromItemPage(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.canOrder(
                userName,
                data.userSurname,
                userEmail,
                userPhone);
    }

//    Негативные сценарии

    @Test
    public void userCanNotOrderWithWrongName(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.canOrder(
                data.badName,
                data.userSurname,
                userEmail,
                userPhone);

        validators.orderNameError.shouldBe(Condition.visible);
//        validators.thirdStepIcon.isEnabled();
//        validators.thirdStepIcon.isDisplayed();
    }

    @Test
    public void userCanNotOrderWithWrongSurname(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.canOrder(
                userName,
                data.badSurname,
                userEmail,
                userPhone);

        validators.orderSurnameError.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotOrderWithWrongEmail(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.canOrder(
                userName,
                data.userSurname,
                data.badEmail,
                userPhone);

        validators.orderEmailError.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotOrderWithWrongPhone(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.canOrder(
                userName,
                data.userSurname,
                userEmail,
                data.badPhone);

        validators.orderPhoneError.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotOrderWithoutAgreeingTermsOfUse(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.addToCartButton.click();
        productPage.goToCartPage.click();
        productPage.goToOrderPage.click();
        productPage.buyerNameInput.val(userName);
        productPage.buyerLastNameInput.val(data.userSurname);
        productPage.buyerEmailInput.val(userEmail);
        productPage.buyerPhoneInput.val(userPhone);
        productPage.deliveryOption.scrollIntoView(true).click();
        productPage.payOption.click();
        productPage.confirmOrderButton.click();

        validators.orderPolicyError.shouldBe(Condition.visible);
    }

    @Test
    public void userCanNotOrderWithEmptyFields(){
        open(baseUrl + "/product-page.html");
        pageBase.userAcceptingCookie();
        productPage.addToCartButton.click();
        productPage.goToCartPage.click();
        productPage.goToOrderPage.click();
        productPage.confirmOrderButton.click();

        validators.orderNameError.shouldBe(Condition.visible);
        validators.orderSurnameError.shouldBe(Condition.visible);
        validators.orderPhoneError.shouldBe(Condition.visible);
        validators.orderDeliveryError.shouldBe(Condition.visible);
        validators.orderPaymentError.shouldBe(Condition.visible);
        validators.orderPolicyError.shouldBe(Condition.visible);
    }
}
