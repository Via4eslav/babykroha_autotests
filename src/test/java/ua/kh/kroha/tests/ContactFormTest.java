package ua.kh.kroha.tests;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import ua.kh.kroha.data.UserData;
import ua.kh.kroha.data.Validators;
import ua.kh.kroha.pages.ContactsPage;
import ua.kh.kroha.pages.PageBase;

import static com.codeborne.selenide.Selenide.open;

public class ContactFormTest extends TestBase {
    private UserData data = new UserData();
    private PageBase pageBase = new PageBase();
    private ContactsPage contactsPage = new ContactsPage();
    private Validators validators = new Validators();

    @Test
    public void userCanContactToAdmin(){
        open(baseUrl + "/contacts");
        pageBase.userAcceptingCookie();
        contactsPage.canContact(
                data.userName,
                data.userEmail,
                data.userPhone,
                data.testMessage);
        pageBase.thanksPopUp.shouldBe(Condition.visible);
    }

//    Негативные сценарии

    @Test
    public void userCanNotContactToAdminWithWrongName(){
        open(baseUrl + "/contacts");
        pageBase.userAcceptingCookie();
        contactsPage.canContact(
                data.badName,
                data.userEmail,
                data.userPhone,
                data.testMessage);
        validators.contactsNameError.shouldBe(Condition.visible);
//        pageBase.thanksPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotContactToAdminWithWrongEmail(){
        open(baseUrl + "/contacts");
        pageBase.userAcceptingCookie();
        contactsPage.canContact(
                data.userName,
                data.badEmail,
                data.userPhone,
                data.testMessage);
        validators.contactsEmailError.shouldBe(Condition.visible);
//        pageBase.thanksPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotContactToAdminWithWrongPhone(){
        open(baseUrl + "/contacts");
        pageBase.userAcceptingCookie();
        contactsPage.canContact(
                data.userName,
                data.userEmail,
                data.badPhone,
                data.testMessage);
        validators.contactsTelError.shouldBe(Condition.visible);
//        pageBase.thanksPopUp.shouldNotBe(Condition.visible);
    }

    @Test
    public void userCanNotContactToAdminWithEmptyFields(){
        open(baseUrl + "/contacts");
        pageBase.userAcceptingCookie();
        contactsPage.submitContactsForm.click();
        validators.contactsNameError.shouldBe(Condition.visible);
        validators.contactsTelError.shouldBe(Condition.visible);
//        pageBase.thanksPopUp.shouldNotBe(Condition.visible);
    }
}
