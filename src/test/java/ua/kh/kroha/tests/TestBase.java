package ua.kh.kroha.tests;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Configuration.startMaximized;
import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.close;

public class TestBase {
    public String baseUrl = "http://1:1@babykroha.wezom.net/_HTML/dist";

    @BeforeClass
    public void start() {
        browser = "chrome";
//        Configuration.headless = true;
//        Configuration.browserSize = "1366x768";
        startMaximized = true;
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        Configuration.timeout = 6000;
        clearBrowserCookies();
    }

    @AfterMethod
    public void tearDown() {
        close();
    }
}


