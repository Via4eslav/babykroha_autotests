package ua.kh.kroha.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public abstract class PageBase1 {

  interface Header {

        //Callback pop-up

      public class CallBackPopUp {
            private static SelenideElement
                    callbackModal = $(".header-phones__callback"),
                    nameInput = $("#reg-name"),
                    phoneInput = $("#tel"),
                    termsCheckBox = $(".cart-registration__checkbox-text"),
                    submitCallBack = $(".login-popup__submit--yellow");

            public static void canGetCallBack(String userName, String userPhone) {
                callbackModal.click();
                nameInput.val(userName);
                phoneInput.val(userPhone);
                termsCheckBox.click();
                submitCallBack.click();
            }
        }

        class SearchModal {
            //Search
            private static SelenideElement
                    searchIcon = $("");

             static void canOpenSearchModal() {
                searchIcon.click();
            }
        }


    }
}
