package ua.kh.kroha.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ProductPage {
    public SelenideElement
            addToCartButton = $(".product__buy-btn");
    private SelenideElement openCartModal = $(".header-cart__btn.js-cart-open");
    public SelenideElement goToCartPage = $(".cart__order-btn");
    public SelenideElement goToOrderPage = $(".cart__total-btn.yellow-btn");
    public SelenideElement//Ordering page locators
            buyerNameInput = $("#name");
    public SelenideElement buyerLastNameInput = $("#surname");
    public SelenideElement buyerPhoneInput = $("#tel");
    public SelenideElement buyerEmailInput = $("#mail");
    private SelenideElement termsCheckBox = $x("//input[@id='agree']");
    public SelenideElement deliveryOption = $("[class] [data-wstabs-ns=\"group-1\"]:nth-of-type(1) .cart-info__delivery-item:nth-child(2) .radio-input__text");
    public SelenideElement payOption = $("[class] [data-wstabs-ns=\"group-1\"]:nth-of-type(1) .cart-info__pay-wrap .cart-info__label:nth-of-type(2)");
    public SelenideElement confirmOrderButton = $("[class] [data-wstabs-ns=\"group-1\"]:nth-of-type(1) .yellow-btn");

    public void canOrder(String userName, String userLastName, String userEmail, String userPhone){
        addToCartButton.click();
        goToCartPage.click();
        goToOrderPage.click();
        buyerNameInput.val(userName);
        buyerLastNameInput.val(userLastName);
        buyerEmailInput.val(userEmail);
        buyerPhoneInput.val(userPhone);
        executeJavaScript("$(\".cart-registration__info .cart-registration__checkbox-label:nth-of-type(6) [type]\").click();");
        deliveryOption.scrollIntoView(true).click();
        payOption.click();
        confirmOrderButton.click();
        /**
         * toDo выбор отделения при помощи селектов
         *
         *
         **/
    }
    public void emptyFields(){
        confirmOrderButton.click();
    }
}
