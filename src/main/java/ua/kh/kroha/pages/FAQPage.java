package ua.kh.kroha.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class FAQPage extends PageBase {

    public SelenideElement faqModal = $(".faq__callback-text"),
    faqTextArea = $("textarea"),
    phoneInput = $("#tel"),
    pageTitleFAQ = $(".main-title.faq__title");

    public FAQPage openFAQModal(){
        faqModal.click();

        return new FAQPage();
    }

    public FAQPage writeMessage(String testMessage){
        faqTextArea.setValue(testMessage);

        return new FAQPage();
    }

}
