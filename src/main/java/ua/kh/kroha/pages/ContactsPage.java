package ua.kh.kroha.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ContactsPage {
    public SelenideElement
            contactsName = $("#contacts-name"),
            contactsEmail = $("#contacts-mail"),
            contactsPhone = $("#contacts-phone"),
            contactsMessage = $("#contacts-msg"),
            submitContactsForm = $(".yellow-btn.contacts__form-submit");

    public ContactsPage canContact(String userName, String userEmail, String userPhone, String testMessage){
        contactsName.val(userName);
        contactsEmail.val(userEmail);
        contactsPhone.val(userPhone);
        contactsMessage.val(testMessage);
        submitContactsForm.click();

        return new ContactsPage();
    }
}
