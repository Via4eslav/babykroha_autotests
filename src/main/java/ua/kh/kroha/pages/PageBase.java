package ua.kh.kroha.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;

public class PageBase {
    //Locators
    //Cookie
    public SelenideElement
            cookieMessage = $(".cookie"),
            acceptCookie = $(".cookie-btn-confirm");

    //Thanks pop-up
    public SelenideElement
            successMessage = $(".thank-popup__text--inner"),
            thanksPopUp = $("#thank-popup"),
            reviewPopUp = $(".popup.review-popup");

    //Callback pop-up
    public SelenideElement
            callbackModal = $(".header-phones__callback"),
            nameInput = $("#reg-name"),
            phoneInput = $(".cart-registration__info [type=\"tel\"]"),
            termsCheckBox = $(".cart-registration__checkbox-text"),
            submitButton = $(".login-popup__submit--yellow"),
            telInput = $("#tel");

    //Search
    public SelenideElement
            searchIcon = $(".header-search__btn");

    //Authorization/Registration pop-up
    public SelenideElement
            userAuthModal = $(".header-login__enter"),
            wholesaleAuthModal = $(".header-wholesale__enter"),
            loginEmailInput = $("#enter-mail"),
            loginPassword = $("#password"),
            loginEnterButton = $(".login-form .login-popup__submit--gray"),

            regNameInput = $("#reg-name"),
            regSurnameInput = $("#reg-surname"),
            regEmailInput = $("#reg-mail"),
            regPassword = $("#reg-password"),
            regRepeatPassword = $("#reg-password-repeat"),
            regTermsCheckBox = $(".cart-registration__checkbox"), //toDo need to change locator or leave js method
            regButton = $(".login-popup__submit--yellow");


    //Methods

    /**
     * Callback
     */

    public void canGetCallBack(String userName, String userPhone) {
        callbackModal.click();
        nameInput.val(userName);
        telInput.val(userPhone);
        termsCheckBox.click();
        submitButton.click();
    }

    public void emptyFieldsInCallbackForm(){
        callbackModal.click();
        submitButton.click();
    }

    /**
     * Search
     */

    public void canOpenSearchModal() {
        searchIcon.click();
    }

    /**
     * Login as user/wholesale buyer
     */

    public void openUserAuthModal(){
        userAuthModal.click();
    }

    public void openWholesaleAuthModal(){
        wholesaleAuthModal.click();
    }

    public void canLoginAsUser(String userEmail, String userPassword) {
        loginEmailInput.val(userEmail);
        loginPassword.val(userPassword);
        loginEnterButton.click();
    }

    /**
     * Register as user/wholesale buyer
     */

    public void canRegisterAsUser(String userName, String userSurname, String userEmail,
                                  String userPhone, String password, String passwordRepeat){
        regNameInput.val(userName);
        regSurnameInput.val(userSurname);
        regEmailInput.val(userEmail);
        phoneInput.val(userPhone);
        regPassword.val(password);
        regRepeatPassword.val(passwordRepeat);
        executeJavaScript("$(\".cart-registration__checkbox\").click();");
        regButton.click();
    }

    public void userAcceptingCookie(){
        cookieMessage.shouldBe(Condition.visible);
        acceptCookie.click();
    }
}
