package ua.kh.kroha.data;

import ua.kh.kroha.pages.PageBase;

public class UserData extends PageBase {

    public static String userName = "Тест";
    public static String userPhone = "990000000";

    public static String userEmail = "test@user.com";
    public static String userSurname = "Тестовый";
    public static String password = "000000";
    public static String passwordRepeat = "000000";
    public static String testMessage = "Hi, this is test message. Keep calm! = )";


// Данные для негативных сценариев
    public static String
        badName = "u2",
        badPhone = "8800",
        badEmail = "test@test",
        badSurname = "2u",
        badPassword = "00000",
        badConfirmationPassword = "QWERTYUIOP{}";
}
