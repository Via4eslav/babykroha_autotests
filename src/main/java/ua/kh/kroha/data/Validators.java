package ua.kh.kroha.data;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Validators {

    public SelenideElement
            modalNameError = $("#reg-name-error"),
            modalTelError = $(".review-popup__inner .cart-registration__label:nth-of-type(2) label"),
            modalSurnameError = $("#reg-surname-error"),
            modalEmailError = $("#reg-mail-error"),
            modalPasswordError = $("#reg-password-error"),
            modalRepeatPasswordError = $("#reg-password-repeat-error"),

            telError = $(".cart-registration__info #tel-error"),
            termsError = $("#remember-error"),

            contactsNameError = $("#contacts-name-error"),
            contactsTelError = $("#contacts-phone-error"),
            contactsEmailError = $("#contacts-mail-error"),

            orderNameError = $("#name-error"),
            orderSurnameError = $("#surname-error"),
            orderEmailError = $("#mail-error"),
            orderPhoneError = $("#tel-error"),
            orderPolicyError = $("#agree-error"),
            orderDeliveryError = $("#delivery-error"),
            orderPaymentError = $("#pay-error"),
            thirdStepIcon = $(".order-info .order-info__item:nth-of-type(3) .order-info__item-icon");
}
