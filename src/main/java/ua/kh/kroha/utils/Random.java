package ua.kh.kroha.utils;

import org.apache.commons.lang3.RandomStringUtils;


public class Random {

    public String generateRandomString(){
        return RandomStringUtils.random(8, "abcdefghjklmnopqrstuvwxyz");
    }

    public String generateRandomEmail(){
        return generateRandomString() + "@" + generateRandomString() + ".com";
    }
}
